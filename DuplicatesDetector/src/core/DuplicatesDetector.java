/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import business.DuplicatesDetectorLogic;
import gui.DuplicatesDetectorForm;

/**
 *
 * @author Matthias
 */
public class DuplicatesDetector {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        DuplicatesDetectorLogic logic = new DuplicatesDetectorLogic();
        DuplicatesDetectorForm gui = new DuplicatesDetectorForm(logic);
        logic.attach(gui);
        gui.setVisible(true);
    }
    
}
