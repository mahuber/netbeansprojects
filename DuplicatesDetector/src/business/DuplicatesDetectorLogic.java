/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import gui.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

/**
 *
 * @author huber
 */
public class DuplicatesDetectorLogic {

    private DuplicatesDetectorForm gui;

    public void attach(DuplicatesDetectorForm gui) {
        this.gui = gui;
    }

    public List<Duplicator> prepareDuplicatesList(ArrayList<File> liste) {
        List<Duplicator> duplicates = new ArrayList<>();
        for (File file : liste) {
            duplicates.add(new Duplicator(file.length(), file.getName(), file.getAbsolutePath()));
        }
        return duplicates;

    }

    
    public void deleteFiles(List<String> list){
        
        
    }
    
    
    public void detectDuplicates(List<Duplicator> liste, CustomModel tableModel) {

        Comparator<Duplicator> best = new Comparator<Duplicator>() {
            @Override
            public int compare(Duplicator o1, Duplicator o2) {
                return o1.hashCode() - o2.hashCode();
            }
        };
        Collections.sort(liste, best);
        Set<Duplicator> uniqueWords = new HashSet<Duplicator>(liste);

        for (Duplicator o1 : liste) {
            System.out.println("Sortierte Liste:" + o1.getFilename());
        }

        for (Duplicator duplicator : uniqueWords) {
            int frequency = Collections.frequency(liste, duplicator);
            if (frequency > 1) {

                System.out.println("mehrfacher Eintrag: " + duplicator.getFilename() + " " + duplicator.getFileSize() + ": " + frequency);

                int i = liste.indexOf(duplicator);
                System.out.println("Index des Duplikats: " + i);
                for (int e = 0; e < frequency; e++) {

                    System.out.println("Duplikats Listen-Eintrag: " + (i + e));
                    Duplicator duplicatorFinal = liste.get((i + e));

                    Vector customData = new Vector();
                    customData.addElement(duplicatorFinal.getFilename());
                    customData.addElement(duplicatorFinal.getPath());
                    customData.addElement(duplicatorFinal.getFileSize());
                    if (e == 0) {
                        customData.addElement(new Boolean(true));
                    } else {
                        customData.addElement(new Boolean(false));
                    }
                    tableModel.insertRow(0, customData);

                }

            }
        }

    }
}
