/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author huber
 */
public class CustomModel extends DefaultTableModel{
    

    //Column-Headers
     Vector customNames = new Vector();
    
   
    public CustomModel() {
      customNames.addElement("Filename");
      customNames.addElement("Path");
      customNames.addElement("Size");
      customNames.addElement("Löschen");
      super.setColumnIdentifiers(customNames);
             
    }


    
    
    @Override
    public void addRow(Object[] rowData) {
        super.addRow(rowData); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addRow(Vector rowData) {
        super.addRow(rowData); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertRow(int row, Vector rowData) {
        super.insertRow(row, rowData); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertRow(int row, Object[] rowData) {
        super.insertRow(row, rowData); //To change body of generated methods, choose Tools | Templates.
    }
   
  
}
