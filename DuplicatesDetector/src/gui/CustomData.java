/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

/**
 *
 * @author huber
 */
public class CustomData {
    String filname;
    String path;
    Long size;
    Boolean checkedForDelete;

    public CustomData(String filname, String path, Long size, Boolean checkedForDelete) {
        this.filname = filname;
        this.path = path;
        this.size = size;
        this.checkedForDelete = checkedForDelete;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    

    public String getFilname() {
        return filname;
    }

    public void setFilname(String filname) {
        this.filname = filname;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

   

    public Boolean getCheckedForDelete() {
        return checkedForDelete;
    }

    public void setCheckedForDelete(Boolean checkedForDelete) {
        this.checkedForDelete = checkedForDelete;
    }
    
    
      
}
