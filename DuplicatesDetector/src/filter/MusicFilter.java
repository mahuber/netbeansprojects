/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;


/**
 *
 * @author huber
 */
public class MusicFilter extends Filter implements Runnable {
 private Thread thread;

    
    
     public void listMP3Files(File directory, ArrayList<File> files, String artistFilter) {

        // get all the mp3 files from a directory
        File[] fList = directory.listFiles();
        for (File file : fList) {
            if (file.isFile()){
                 System.out.println("----File: "+file.getName());
                 boolean gefundenMitFilenamen = filterFilenameDecision(file, artistFilter);
                 boolean gefundenMitID3Tag = filterArtistFromMP3File(file, artistFilter);
               if(gefundenMitFilenamen || gefundenMitID3Tag) {
                 files.add(file);
               }
            } else if (file.isDirectory()) {
                System.out.println(file.getName()+" ist Verzeichnis");
                listMP3Files(file, files, artistFilter);
            }
        }

    }
    
    
     
    public static boolean filterArtistFromMP3File(File file, String artistFilter) {

        if (file.getName().endsWith(".mp3") && !artistFilter.isEmpty()) {
                try {
                    Mp3File mp3file = new Mp3File(file);
                    if (mp3file.hasId3v2Tag()) {
                        ID3v2 id3v2Tag = mp3file.getId3v2Tag();
                        System.out.println("ID3 TAG SEARCH: ID3-Tag Artist: " + id3v2Tag.getArtist());
                        if (id3v2Tag.getArtist().toLowerCase().contains(artistFilter.toLowerCase())) {
                            System.out.println("ID3 TAG SEARCH: Gefunden mit Endung und ID3 Tag");
                            return true;
                        } else {
                            System.out.println("ID3 TAG SEARCH: Kein ID3 Tag Matching");
                            return false;
                        }
                    } else {
                        System.out.println("ID3 TAG SEARCH: Kein ID3 Tag vorhanden");
                        return false;
                    }
                } catch (IOException ex) {
                    Logger.getLogger(MusicFilter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (UnsupportedTagException ex) {
                    Logger.getLogger(MusicFilter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvalidDataException ex) {
                    Logger.getLogger(MusicFilter.class.getName()).log(Level.SEVERE, null, ex);
                }
        } 
         else if(file.getName().endsWith(".mp3") && artistFilter.isEmpty()){
             System.out.println("ID3 TAG SEARCH: Gefunden mit Endung");
             return true;
        }
        System.out.println("ID3 TAG SEARCH: Nicht gefunden mit ID3 Tag");
        return false;
    }

   

    public static boolean filterFilenameDecision(File file, String artistFilter) {
        if (!artistFilter.isEmpty()) {
            if (file.getName().endsWith(".mp3")
                    && file.getName().toLowerCase().contains(artistFilter.toLowerCase())) {
                System.out.println("FILENAME SEARCH: Gefunden mit Endung und Filenamen");
                return true;
            }
        } else if (artistFilter.isEmpty()) {
            if (file.getName().endsWith(".mp3")) {
                 System.out.println("FILENAME SEARCH: Gefunden mit Endung");
                return true;
            }

        }
     return false;
    }
    
    @Override
    public void run() {
             
    }
}
