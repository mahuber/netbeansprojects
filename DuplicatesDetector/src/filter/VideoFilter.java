/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import static filter.MusicFilter.filterArtistFromMP3File;
import static filter.MusicFilter.filterFilenameDecision;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author huber
 */
public class VideoFilter extends Filter implements Runnable{

    
    public void listVideoFiles(File directory, ArrayList<File> files, String artistFilter) {

        // get all the mp3 files from a directory    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        File[] fList = directory.listFiles();
        for (File file : fList) {
            if (file.isFile()){
                 System.out.println("----GUGUS File: "+file.getName());
                 boolean gefundenMitFilenamen = filterFilenameDecision(file, artistFilter);
                 if(gefundenMitFilenamen) {
                 files.add(file);
                 }
               }

             else if (file.isDirectory()) {
                System.out.println(file.getName()+" ist Verzeichnis");
                listVideoFiles(file, files, artistFilter);
            }
        }

    }
    
    
    public static boolean filterFilenameDecision(File file, String artistFilter) {
        if (!artistFilter.isEmpty()) {
            if ((file.getName().endsWith(".wmv") || file.getName().endsWith(".mpg")|| 
                    file.getName().endsWith(".mp4")|| file.getName().endsWith(".mov")|| file.getName().endsWith(".avi")
                    || file.getName().endsWith(".flv")|| file.getName().endsWith(".rm")|| file.getName().endsWith(".divx")
                    || file.getName().endsWith(".swf"))
                    && file.getName().toLowerCase().contains(artistFilter.toLowerCase())) {
                System.out.println("VIDEO FILENAME SEARCH: Gefunden mit Endung und Filenamen");
                return true;
            }
        }
            else if (artistFilter.isEmpty()) {
                System.out.println("TEST VIDEO NAME:"+file.getName());
            if (file.getName().endsWith(".wmv") || file.getName().endsWith(".mpg")|| 
                    file.getName().endsWith(".mp4")|| file.getName().endsWith(".mov")|| file.getName().endsWith(".avi")
                    || file.getName().endsWith(".flv")|| file.getName().endsWith(".rm")|| file.getName().endsWith(".divx")
                    || file.getName().endsWith(".swf")) {
                 System.out.println("VIDEO FILENAME SEARCH: Gefunden mit Endung");
                return true;
            }

         }
        return false;
   
    }
    
    
    @Override
    public void run() {
     int i = 0;
       while(true){
           if(Thread.interrupted()){
               System.out.println("Thread gestoppt");
               break;
           }
          // System.out.println(i++);
       }
    }

  
    
}
